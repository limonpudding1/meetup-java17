package ru.stroki.meetup.java17;

public sealed interface Pattern permits PatternImpl1, PatternImpl2 {

  int getValue();
}
