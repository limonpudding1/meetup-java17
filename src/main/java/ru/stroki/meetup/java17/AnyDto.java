package ru.stroki.meetup.java17;

public record AnyDto(Integer number, String text, Boolean active) {}
